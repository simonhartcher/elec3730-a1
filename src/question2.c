#include "question2.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "question1.h"

int 
load_data(FILE *fp, t_data *ptr_data) {
  //read into array
  fread(&ptr_data->n, BINARY_N_LENGTH, 1, fp);

  //get n
  int n = extract_little((char *)&ptr_data->n, 0, BINARY_N_LENGTH);

  //allocate array
  ptr_data->array = malloc(sizeof(t_double) * n);

  //read into array
  fread(ptr_data->array, sizeof(double), n, fp);

  return n;
}

void 
print_data(const char *filePath) {
  //open file
  FILE *fp = fopen(filePath, "rb");

  //return if failed to open
  if (fp == 0) {
    printf("File could not be opened.");
    return;
  }

  //get data
  t_data fd;
  int n = load_data(fp, &fd);

  //get file size
  fseek(fp, 0L, SEEK_END);
  int size = ftell(fp);
  fseek(fp, 0L, SEEK_SET);

  //check with expected
  int expected_size = BINARY_N_LENGTH + (n * sizeof(double));
  if (size < expected_size) {
    printf("Error: File size smaller than expected.\n");
    return;
  }

  //print file data
  printf("Data written on the file:\n");
  printf("n =  %d\n", n);
  printf("array =\n\n");

  int i;
  double *ptr_array = fd.array;
  for (i = 0; i < n; i++) {
    double d = (*ptr_array++);
    char* padding = (d > 0) ? "   " : "  ";
    printf("%s%f\n", padding, d);
  }

  //free objects
  free(fd.array);
  fclose(fp);
}

int
main(int argc, char *argv[]) {
  if (argc != 2) {
    fprintf(stdout, "Usage: %s <path>\n", argv[0]);
    return 1;
  }

  print_data(argv[1]);
}
