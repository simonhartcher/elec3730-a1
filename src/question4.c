#include "question4.h"
#include "question1.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum {
  BINARY_N_LENGTH = 4,
  BINARY_DOUBLE_SIZE = 8
};

typedef char t_int_le[BINARY_N_LENGTH];
typedef char t_double[BINARY_DOUBLE_SIZE];
typedef struct data {
  t_int_le n;
  double *array;
} t_data;

enum {
  WAV_OFFSET_DESC_CHUNK = 0,
  WAV_OFFSET_FMT_CHUNK = 12,
  WAV_OFFSET_DATA_CHUNK = 36,
  WAV_OFFSET_DATA_START = 44
};

typedef struct riff_chunk_desc {
  char id[4];
  int size;
  char format[4];
} DescChunk;

typedef struct fmt_sub_chunk {
  char id[4];
  int size;
  short audio_format;
  short num_channels;
  int sample_rate;
  int byte_rate;
  short block_alignment;
  short bits_per_sample;
} FormatChunk;

typedef struct data_sub_chunk {
  char id[4];
  int size;
  double *data;
} DataChunk;

typedef t_data FirData;

int 
load_data(FILE *fp, t_data *ptr_data) {
  //read into array
  fread(&ptr_data->n, BINARY_N_LENGTH, 1, fp);

  //get n
  int n = extract_little((char *)&ptr_data->n, 0, BINARY_N_LENGTH);

  //allocate array
  ptr_data->array = malloc(sizeof(t_double) * n);

  //read into array
  fread(ptr_data->array, sizeof(double), n, fp);

  return n;
}

void 
apply_filter(double *coeffs, unsigned char *in, unsigned char *out, int length, 
  int filter_length) {

  int n, k;
  for (n = 0; n < length; n++) {
    double res = 0;

    for (k = 0; k < filter_length; k++) {
      res += (coeffs[k] * in[n - k]);
    }
    #if DEBUG
    printf("Before: %u, After: %f\n", in[n - k], res);
    #endif
    out[n] = (unsigned char)res;
  }
}

void 
filter_wav(char *wav_path, char *filter_path, char *out_path) {
  //open wav file
  FILE *in_file = fopen(wav_path, "rb");

  //return if failed to open
  if (in_file == 0) {
    printf("Wav file could not be opened.");
    return;
  }

  //load headers into buffer
  char buf[44];
  fread(buf, 44, 1, in_file);

  //load the data chunk of the wav file
  DataChunk *wav_data = (DataChunk *)(buf + WAV_OFFSET_DATA_CHUNK);
  int chunkSize = wav_data->size;

  #if DEBUG
  printf("Data Chunk Size: %d\n", chunkSize);
  #endif

  //open filter file
  FILE *filter_file = fopen(filter_path, "rb");

  //return if failed to open
  if (filter_file == 0) {
    printf("FIR file could not be opened.");
    return;
  }

  FirData fir_data;
  int n = load_data(filter_file, &fir_data);

  //write to new file
  FILE *out_file = fopen(out_path, "wb");

  //write header from previous file
  fwrite(&buf, sizeof(char), 44, out_file);

  //get sample size in bits
  FormatChunk *wav_fmt = (FormatChunk *)(buf + WAV_OFFSET_FMT_CHUNK);
  //n in converted to bits
  int sample_size = wav_fmt->bits_per_sample * n * 8;

  //init buffers
  unsigned char in[sample_size];
  unsigned char out[sample_size];

  //get wav data
  int read;
  do {
    read = fread(in, sizeof(char), sample_size, in_file);

    apply_filter(fir_data.array, in, out, read, n);

    fwrite(out, sizeof(char), read, out_file);
  } while (read != 0);

  //free objects
  free(fir_data.array);
  fclose(filter_file);
  fclose(in_file);
  fclose(out_file);
}

int
main(int argc, char *argv[]) {
  if (argc != 4) {
    fprintf(stdout, "Usage: %s <wav in> <filter in> <wav out>\n", argv[0]);
    return 1;
  }

  filter_wav(argv[1], argv[2], argv[3]);
}
