/*
  @author Simon Hartcher
  @studentNo c3185790
 */

#include <fcntl.h>
#include <unistd.h>
#include <alt_types.h>
#include <stdio.h>

//for compiling on windows
#ifndef O_NONBLOCK
  #define O_NONBLOCK 0
#endif

#define UART_FILE "/dev/uart_0"

int main() {
  //get file descriptors
  alt_32 uart_read = open(UART_FILE, O_RDONLY | O_NONBLOCK);
  alt_32 uart_write = open(UART_FILE, O_WRONLY);

  alt_8 c;
  do {
    //read character
  	c = 0;
    read(uart_read, &c, sizeof(alt_8));

    //write character
    if (c > 0) {
    	write(uart_write, &c, sizeof(alt_8));
    	printf("%c", c);
    }

  } while (c != 0xd);

  if (c == 0xd) {
	  alt_8 *message = "\nEnter pressed... exiting.\n";
	  write(uart_write, message, strlen(message));
	  printf("%s", message);
  }
}
