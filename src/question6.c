/*
  @author Simon Hartcher
  @studentNo c3185790
 */

#include <stdio.h>
#include <conio.h>

int main() {
  int c;
  do {

    c = getch();
    printf("%d\n", c);

  } while (c != 0xd);
}
