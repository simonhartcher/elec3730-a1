/*
  @author Simon Hartcher
  @studentNo c3185790
 */
#include "question1.h"
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if CHAR_BIT != 8
#error "unsupported char size"
#endif
  
enum
{
  O32_LITTLE_ENDIAN = 0x03020100ul,
  O32_BIG_ENDIAN = 0x00010203ul,
  O32_PDP_ENDIAN = 0x01000302ul
};

static const union { unsigned char bytes[4]; uint32_t value; } o32_host_order =
    { { 0, 1, 2, 3 } };

#define O32_HOST_ORDER (o32_host_order.value)

static inline bool
will_overflow(int n, int offset, int len) {
  int int_size = sizeof(int);
  if (n > int_size) {
    fprintf(stderr, "Error: Number of bytes exceeded. Int size: %d > %d.\n", int_size, n);
    return true;
  }

  #if DEBUG
  printf("N = %d, Offset = %d, Length = %d \n", n, offset, len);
  #endif

  return false;
}

void 
reverse(char bytes[], int n) {
  int i = 0;
  int j = n - 1;
  while (j - i > 0)
  {
    char temp = bytes[j];
    bytes[j] = bytes[i];
    bytes[i] = temp;

    i++;
    j--;
  }
}

int
read_bytes(char *str, int n) {
  int res = 0;

  int i;
  for (i = 0; i < n; ++i)
  {
    int val = (int)str[i];
    res = res | (val << (i * 8));
  }

  return res;
}

void 
print_bytes(char bytes[], int n) {
    int k;
    for (k = 0; k < n; ++k)
    {
      printf("%d ", bytes[k]);
    }
    printf("\n");
}

int 
extract_endian(char *str, int offset, int n, uint32_t source) {
   if (!will_overflow(n, offset, strlen(str))) {
    char bytes[n];
    memcpy(&bytes, &str[offset], n * sizeof(char));

    #if DEBUG
    printf("Before: ");
    print_bytes(bytes, n);
    #endif
    if (O32_HOST_ORDER != source) {
      reverse(bytes, n);
    }
    #if DEBUG
    printf("After: ");
    print_bytes(bytes, n);
    #endif

    return read_bytes((char *)&bytes, n);
  }
  return 0; 
}

int 
extract_little(char *str, int ofset, int n) {
  return extract_endian(str, ofset, n, O32_LITTLE_ENDIAN);
}

int 
extract_big(char *str, int ofset, int n) {
  return extract_endian(str, ofset, n, O32_BIG_ENDIAN);
}
