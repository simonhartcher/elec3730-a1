/*
  @author Simon Hartcher
  @studentNo c3185790
 */

#ifndef QUESTION2_H
#define QUESTION2_H
#include <stdio.h>

enum {
  BINARY_N_LENGTH = 4,
  BINARY_DOUBLE_SIZE = 8
};

typedef char t_int_le[BINARY_N_LENGTH];
typedef char t_double[BINARY_DOUBLE_SIZE];
typedef struct data {
  t_int_le n;
  double *array;
} t_data;

void print_data(const char *filePath);
int load_data(FILE *fp, t_data *ptr_data);

#endif