#include "question5.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

char 
string_parser(char* inp, char* array_of_words[]) {
  if (strlen(inp) == 0 || inp == NULL) {
    return 0;
  }
  if (array_of_words == NULL) {
    return 0;
  }

  //create temp string
  char temp[1 + strlen(inp)];
  strcpy(temp, inp);

  int i = 0;
  char *word = strtok(temp, " ");
  while (word != NULL) {
    array_of_words[i] = malloc(strlen(word) + 1);
    strcpy(array_of_words[i], word);

    word = strtok(NULL, " \0");
    i++;
  }

  return i;
}