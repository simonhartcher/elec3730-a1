/*
  @author Simon Hartcher
  @studentNo c3185790
 */

#ifndef QUESTION1_H
#define QUESTION1_H

int extract_little(char *str, int ofset, int n);
int extract_big(char *str, int ofset, int n);

#endif