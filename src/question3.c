#include "question3.h"
#include "question1.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

void 
print_wav_headers(const char *filePath) {
  //open file
  FILE *fp = fopen(filePath, "r");

  //return if failed to open
  if (fp == 0) {
    printf("File could not be opened.");
    return;
  }

  int buf_size = 44;
  char buf[buf_size];
  fread(buf, buf_size, 1, fp);

  printf("Reading Wav File: %s\n\n", filePath);

  DescChunk *desc = (DescChunk *)(buf + WAV_OFFSET_DESC_CHUNK);
  printf("Chunk Id: %.4s\n", desc->id);
  printf("Chunk Size: %d\n", desc->size);
  printf("Format: %.4s\n\n", desc->format);

  FormatChunk *fmt = (FormatChunk *)(buf + WAV_OFFSET_FMT_CHUNK);
  printf("Chunk Id: %.4s\n", fmt->id);
  printf("Chunk Size: %d\n", fmt->size);
  printf("Audio Format: %d\n", fmt->audio_format);
  printf("# Channels: %d\n", fmt->num_channels);
  printf("Sample Rate: %d\n", fmt->sample_rate);
  printf("Byte Rate: %d\n", fmt->byte_rate);
  printf("Block Alignment: %d\n", fmt->block_alignment);
  printf("Bits Per Sample: %d\n\n", fmt->bits_per_sample);

  DataChunk *data = (DataChunk *)(buf + WAV_OFFSET_DATA_CHUNK);
  printf("Chunk Id: %.4s\n", data->id);
  printf("Chunk Size: %d\n\n", data->size);

  //free data
  fclose(fp);
}

int
main(int argc, char *argv[]) {
  if (argc != 2) {
    fprintf(stdout, "Usage: %s <path>\n", argv[0]);
    return 1;
  }

  print_wav_headers(argv[1]);
}