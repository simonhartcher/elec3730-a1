/*
  @author Simon Hartcher
  @studentNo c3185790
 */

#ifndef QUESTION3_H
#define QUESTION3_H

enum {
  WAV_OFFSET_DESC_CHUNK = 0,
  WAV_OFFSET_FMT_CHUNK = 12,
  WAV_OFFSET_DATA_CHUNK = 36,
  WAV_OFFSET_DATA_START = 44
};

typedef struct riff_chunk_desc {
  char id[4];
  int size;
  char format[4];
} DescChunk;

typedef struct fmt_sub_chunk {
  char id[4];
  int size;
  short audio_format;
  short num_channels;
  int sample_rate;
  int byte_rate;
  short block_alignment;
  short bits_per_sample;
} FormatChunk;

typedef struct data_sub_chunk {
  char id[4];
  int size;
  double *data;
} DataChunk;

void print_wav_headers(const char *filePath);

#endif