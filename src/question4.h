/*
  @author Simon Hartcher
  @studentNo c3185790
 */

#ifndef QUESTION4_H
#define QUESTION4_H

void filter_wav(char *wav_path, char *filter_path, char *out_path);

#endif