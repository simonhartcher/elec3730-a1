param([string]$projectpath, [string]$outpath)

new-item -type directory -force $outpath | out-null
set-location $outpath
if (!(test-path "$outpath/build.ninja")) {
  invoke-expression "cmake -GNinja $projectpath"
}
invoke-expression 'ninja'